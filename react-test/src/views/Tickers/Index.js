import React, { useEffect, useState  } from 'react';

import '../../styles/general.css';
import tickerServices from "../../services/tickerServices"

import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import ButtonNav from "../Components/ButtonNav"

export default function Index(props) {

  const [ listTickers, setListTickers ] = useState([]);
  const [ ticker, setTicker ] = useState([]);
  const [ isLoading, setLoading ] = useState(false);

  useEffect(()=>{
    loadData();
  },[]);

  const loadData = async () => {
    try {
      const res = await tickerServices.getAll("");
      setListTickers(res.data)
      console.log("Resultado")
      console.log(res.data)
    } catch (e) {
      console.log(e.message)
    }
  }

  const onClickDetails = async (data) => {

    try {
      const res = await tickerServices.get("?id="+data.id);
      console.log("Resultado")
      console.log(res.data)
      alert(JSON.stringify(res))
    } catch (e) {
      console.log(e.message)
    }
  }

  return (
    <div className="container">
      <header className="App-header">
         <h1>Coins</h1>
         <ButtonNav setMenu={props.setMenu}/>
       </header>

      <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="right">ID</TableCell>
            <TableCell align="right">Criptomoneda</TableCell>
            <TableCell align="right">Precio USD</TableCell>
            <TableCell align="right">1h</TableCell>
            <TableCell align="right">24h</TableCell>
            <TableCell align="right">7d</TableCell>
            <TableCell align="right">Acciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {listTickers.map((row) => (
            <TableRow key={row.name} sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
              <TableCell > {row.id} </TableCell>
              <TableCell > {row.name} </TableCell>
              <TableCell align="right">${row.price_usd}</TableCell>
              <TableCell align="right">{row.percent_change_1h}%</TableCell>
              <TableCell align="right">{row.percent_change_24h}%</TableCell>
              <TableCell align="right">{row.percent_change_7d}%</TableCell>
              <TableCell align="right">
                <Button variant="contained" onClick={()=>props.setCoin(row)}>Market</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>


    </div>
  );
}
