import React, { useEffect, useState  } from 'react';

import '../styles/main.css';
import Tickers from './Tickers/Index'
import Exchange from './Exchange/Index'
import Coins from './Coins/Index'

import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';

function Main() {

  const [value, setValue] = useState(1);
  const [ coin , setCoin ] = useState({
    id:0
  });

  const setMenu = (num) => {
    setValue(num)
  }

  const setDataCoin = (data) => {
    setCoin(data)
    setMenu(3)
  }

  return (
    <div>
      {
        value==1?
        <Tickers setMenu={(num)=>setMenu(num)} setCoin={(data)=>setDataCoin(data)}/>
        :value==2?
        <Exchange setMenu={(num)=>setMenu(num)} />
        :value==3?
        <Coins setMenu={(num)=>setMenu(num)} coin={coin}/>
        :null
      }
    </div>
  );
}

export default Main;
