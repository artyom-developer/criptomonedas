import React, { useEffect, useState  } from 'react';

import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

export default function ButtonNav(props) {

  const [ listMenu, setListMenu ]  = useState([
    { id:1, label: "Coin's" },
    { id:2, label: "Exchange" },
    // { id:3, label: "Market" }
  ]);

  const onClickMenu = (menu) => {
    props.setMenu(menu)
  }

  return (
    <PopupState variant="popover" popupId="demo-popup-menu">
      {(popupState) => (
        <React.Fragment>
          <Button variant="contained" {...bindTrigger(popupState)} style={{backgroundColor:"#4E00CC"}}>
            Menu
          </Button>
          <Menu {...bindMenu(popupState)}>
            {
              listMenu.map((item)=>{
                return(
                  <MenuItem onClick={()=>onClickMenu(item.id)}>{item.label}</MenuItem>
                )
              })
            }
          </Menu>
        </React.Fragment>
      )}
    </PopupState>
  );
}
