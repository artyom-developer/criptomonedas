import React, { useEffect, useState  } from 'react';

import '../../styles/general.css';
import coinServices from "../../services/coinServices"

import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import ButtonNav from "../Components/ButtonNav"

export default function Index(props) {

  const [ listMarket, setListMarket ] = useState([]);
  const [ ticker, setTicker ] = useState([]);
  const [ isLoading, setLoading ] = useState(false);

  useEffect(()=>{
    loadData();
  },[]);

  const loadData = async () => {
    try {
      const res = await coinServices.getMarketByCoin(props.coin.id);
      setListMarket(res)
      console.log("Resultado")
      console.log(res)
    } catch (e) {
      console.log(e.message)
    }
  }

  return (
    <div className="container">
      <header className="App-header">
         <h1>Market {props.coin.name}</h1>
         <Button variant="outlined" onClick={()=>props.setMenu(1)}>Atras</Button>
       </header>

      <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell>Base</TableCell>
            <TableCell>Cita</TableCell>
            <TableCell>Precio</TableCell>
            <TableCell>Precio Dolar</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            listMarket.map((row) => (
              <TableRow key={row.name} sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                <TableCell > {row.name} </TableCell>
                <TableCell > {row.base} </TableCell>
                <TableCell > {row.quote} </TableCell>
                <TableCell > {row.price} </TableCell>
                <TableCell > {row.price_usd} </TableCell>
              </TableRow>
            ))
        }
        </TableBody>
      </Table>
    </TableContainer>


    </div>
  );
}
