import React, { useEffect, useState  } from 'react';

import '../../styles/general.css';
import exchangeServices from "../../services/exchangeServices"
import Button from '@mui/material/Button';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import ButtonNav from "../Components/ButtonNav"

export default function Index(props) {

  const [ listExchange, setListExchange ] = useState([]);
  const [ change, setExchange ] = useState([]);
  const [ isLoading, setLoading ] = useState(false);

  useEffect(()=>{
    loadData();
  },[]);

  const loadData = async () => {
    try {
      const res = await exchangeServices.getAll();
      // setListExchange(res.data)
      const object  = res
      const list = []

      console.log("Resultado")
      for (const property in object) {
        list.push(object[property])
      }
      setListExchange(list)
    } catch (e) {
      console.log(e.message)
    }
  }

  const onClickDetails = async (data) => {

    try {
      const res = await exchangeServices.get("?id="+data.id);
      console.log("Resultado")
      console.log(res.data)
      alert(JSON.stringify(res))
    } catch (e) {
      console.log(e.message)
    }
  }

  return (
    <div className="container">
      <header className="App-header">
         <h1>Exchange</h1>
         <ButtonNav setMenu={props.setMenu}/>
       </header>

      <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="right">ID</TableCell>
            <TableCell align="right">Intercambio</TableCell>
            <TableCell align="right">Pais</TableCell>
            <TableCell align="right">fecha</TableCell>
            <TableCell align="right">Web page</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            listExchange.map((row) => (
              <TableRow key={row.name} sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                <TableCell > {row.id} </TableCell>
                <TableCell > {row.name} </TableCell>
                <TableCell > {row.country} </TableCell>
                <TableCell > {row.date_live} </TableCell>
                  <TableCell > {row.url} </TableCell>
              </TableRow>
            ))
        }
        </TableBody>
      </Table>
    </TableContainer>
 
    </div>
  );
}
