const coin = {};

const axios = require('axios');
const env = require('../env');

const urlCoins = env.baseUrl+"coin"

coin.getMarketByCoin = async (id) => {

  const url = urlCoins+"/markets/?id="+id
  const res = await axios.get(url)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}

export default coin
