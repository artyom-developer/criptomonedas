const tickers = {};

const axios = require('axios');
const env = require('../env');
// import env from "../env"

const urlTickers = env.baseUrl+"ticker"

tickers.getAll = async (parameter) => {

  const url = urlTickers+"s/"+parameter
  const res = await axios.get(url)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}

tickers.get = async (parameter) => {

  const url = urlTickers+"/"+parameter
  const res = await axios.get(url)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}


export default tickers
