const exchange = {};

const axios = require('axios');
const env = require('../env');
// import env from "../env"

const urlExchange = env.baseUrl+"exchange"

exchange.getAll = async (parameter) => {

  const url = urlExchange+"s/"
  console.log(url)
  const res = await axios.get(url)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}

exchange.get = async (parameter) => {

  const url = urlExchange+"/"+parameter
  const res = await axios.get(url)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}


export default exchange
